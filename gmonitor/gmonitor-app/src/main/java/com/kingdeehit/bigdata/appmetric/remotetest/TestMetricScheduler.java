package com.kingdeehit.bigdata.appmetric.remotetest;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kingdeehit.bigdata.appmetric.model.AgrOper;
import com.kingdeehit.bigdata.appmetric.schedule.MetricSchedule;
import com.kingdeehit.bigdata.appmetric.schedule.MetricStatistic;


public class TestMetricScheduler {
    private static final Logger log = LoggerFactory.getLogger(TestMetricScheduler.class);
    private MetricStatistic statistic;
    private ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);

    public static void main(String[] args) throws InterruptedException {
        TestMetricScheduler scheduler = new TestMetricScheduler();
        if (args[0].equals("single1")) {
            scheduler.init(args[1]);
            log.info("single1 thread!!!");
            scheduler.singleThreadSche();
        } else if (args[0].equals("single2")) {
            scheduler.init(args[1]);
            log.info("single2 thread!!!");
            scheduler.singleThreadSche();
        } else {
            scheduler.multiThreadSche();
        }
    }

    private void init(String bussiness) {
        // servlet 初始化执行下列操作 调度和指标注册
        statistic = new MetricStatistic(bussiness);
        // 注册指标，和对指标要执行的操作
        statistic.register("pv", AgrOper.SUM);
        statistic.register("avgpv", AgrOper.AVG);
        statistic.register("maxpv", AgrOper.MAX);
        statistic.register("avgsum", AgrOper.AVGSUM);
        // 开始调度
        MetricSchedule schedule = MetricSchedule.builder().setIntervalSecond(5l).setStatstic(statistic)
.build();
        schedule.start();
    }

    private void singleThreadSche() throws InterruptedException {
        // 单线程业务操作
        for (int i = 1; i < 100000; i++) {
            if (i % 100 == 0) {
                // log.info("sleep start:" + i);
                Thread.sleep(1000);
                // log.info("sleep over:" + i);
            }
            try {
                statistic.record("pv", 1l);
                statistic.record("avgpv", 1l);
                statistic.record("maxpv", 1l);
                statistic.record("avgsum", 5l);
            } catch (Exception e) {
                log.error("record metric error", e);
            }
        }
    }

    private void multiThreadSche() {
        // 多线程业务操作
        for (int i = 0; i < 20; i++) {
            executor.submit(new SchedulerTask());
        }
    }

    public class SchedulerTask implements Callable {
        @Override
        public Object call() throws Exception {
            singleThreadSche();
            return null;
        }
    }
}
