package com.kingdeehit.bigdata.appmetric.utils;

import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

//该工具类只�?�合redis集群模式
public class JedisClusterUtil {

    private static final Logger log = LoggerFactory.getLogger(JedisClusterUtil.class);
    private static String REDISCONF = "redis_pool.properties";
    private static JedisCluster jc;

    private JedisClusterUtil() {

    }

    static {
        Set<HostAndPort> nodes = new HashSet<>();
        Properties properties = new Properties();
        try {
            properties.load(JedisClusterUtil.class.getClassLoader().getResourceAsStream(REDISCONF));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        String[] redisNodes = properties.getProperty("redis.cluster.nodes").split(";");
        for (String node : redisNodes) {
            String[] hostAndPort = node.split(":");
            HostAndPort hp = new HostAndPort(hostAndPort[0], Integer.parseInt(hostAndPort[1]));
            nodes.add(hp);
        }
        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
        poolConfig.setMaxIdle(Integer.parseInt(properties.getProperty("redis.pool.maxIdle")));
        poolConfig.setMaxWaitMillis(Long.valueOf(properties.getProperty("redis.pool.maxWait")));
        poolConfig.setMaxTotal(Integer.parseInt(properties.getProperty("redis.pool.maxTotal")));
        poolConfig.setTestOnBorrow(Boolean.getBoolean(properties.getProperty("redis.pool.testOnBorrow")));
        poolConfig.setTestOnReturn(Boolean.getBoolean(properties.getProperty("redis.pool.testOnReturn")));
        jc = new JedisCluster(nodes, poolConfig);
    }

    public static JedisCluster getJedisCulster() {
        return jc;
    }
}
