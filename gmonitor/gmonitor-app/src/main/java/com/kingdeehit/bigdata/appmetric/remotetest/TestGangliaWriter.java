package com.kingdeehit.bigdata.appmetric.remotetest;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;
import com.kingdeehit.bigdata.appmetric.ganglia.GangliaWriter;

public class TestGangliaWriter {
    private static final Logger log = LoggerFactory.getLogger(TestGangliaWriter.class);

    public static void main(String[] args) throws UnknownHostException {
        if (args[0].equals("write")) {
            write(args);
        } else if (args[0].equals("hostname")) {
            log.info(InetAddress.getLocalHost().getHostAddress());
            log.info(InetAddress.getLocalHost().getCanonicalHostName());
            log.info(InetAddress.getLocalHost().getHostName());
        }
    }

    public static void write(String[] args) {
        String host = args[1];
        Integer port = Integer.parseInt(args[2]);
        GangliaWriter writer = GangliaWriter.builder().setHost(host).setPort(port).setAddressingMode(null).setDmax(null).setTmax(null).setUnits(null)
                .setSlope(null).setTtl(null).setV31(null).setGroupName("test5").build();
        log.info("++++JSON:" + writer.toString());
        Map<String, String> map = Maps.newHashMap();
        map.put("a", "7777");
        map.put("b", "7777");
        writer.doWrite(map);
    }

    public static void read() {

    }
}
