package com.kingdeehit.bigdata.appmetric.remotetest;

import java.util.Arrays;
import java.util.List;

import com.kingdeehit.bigdata.appmetric.flume.GangliaFlumeClient;

public class TestFlumeClient {

    public static void main(String[] args) {
        GangliaFlumeClient client = new GangliaFlumeClient();
        client.init();

        List<String> list = Arrays.asList(new String[] { "dingke", "ddddd", "dddd", "aaaa", "bbbb" });
        client.sendListToFlume(list);
    }
}
