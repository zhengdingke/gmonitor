package com.kingdeehit.bigdata.appmetric.model;

public enum AgrOper {
    SUM, AVGSUM, AVG, MAX
}
