package com.kingdeehit.bigdata.appmetric.schedule;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;
import com.kingdeehit.bigdata.appmetric.model.AgrOper;
import com.kingdeehit.bigdata.appmetric.utils.CachedClient;
import com.kingdeehit.bigdata.appmetric.utils.JedisSingleUtil;

public class MetricStatistic {

    private static final Logger log = LoggerFactory.getLogger(MetricStatistic.class);

    private String serviceName;
    private Long intervalSecond;

    private Map<String, String> itemMap = Maps.newHashMap();
    private CachedClient cachedClient = JedisSingleUtil.getCachedClient();

    public MetricStatistic(String serviceName) {
        this.serviceName = serviceName;
    }

    public void register(String metricName, AgrOper oper) {
        itemMap.put(metricName, oper.toString());
    }

    public void record(String metricName, long num) {
        try {
            switch (itemMap.get(metricName)) {
            case "SUM":
                sum(metricName, num);
                break;
            case "AVGSUM":
                avgsum(metricName, num);
                break;
            case "AVG":
                avg(metricName, num);
                break;
            case "MAX":
                max(metricName, num);
                break;
            default:
                break;
            }
        } catch (Exception e) {
            log.error("record msg error", e);
        }

    }

    private void sum(String metricName, long num) throws Exception {
        cachedClient.incrBy(serviceName + "_" + metricName, num);
    }

    private void avg(String metricName, long num) throws Exception {
        cachedClient.incrBy(serviceName + "_" + metricName, num);
    }

    private void max(String metricName, long num) throws Exception {
        String metricKey = serviceName + "_" + metricName;
        String prev = cachedClient.getCached(metricKey);
        if (StringUtils.isBlank(prev) || Long.parseLong(prev) < num) {
            cachedClient.setCached(metricKey, num + "");
        }
    }

    public void avgsum(String metricName, long num) throws Exception {
        cachedClient.incrBy(serviceName + "_" + metricName, num);
    }

    public void reset() {
        itemMap.keySet().stream().forEach(k -> {
            try {
                cachedClient.setCached(serviceName + "_" + k, "0");
            } catch (Exception e) {
                log.error("redis resetmetric {} error", k, e);
            }
        });
    }

    public String getServiceName() {
        return serviceName;
    }

    public Map<String, String> getItemMap() {
        return itemMap;
    }

    public CachedClient getCachedClient() {
        return cachedClient;
    }

    public Long getIntervalSecond() {
        return intervalSecond;
    }

    public void setIntervalSecond(Long intervalSecond) {
        this.intervalSecond = intervalSecond;
    }

}
