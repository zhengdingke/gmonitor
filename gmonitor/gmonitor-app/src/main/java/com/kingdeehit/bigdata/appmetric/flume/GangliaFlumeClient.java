package com.kingdeehit.bigdata.appmetric.flume;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.flume.Event;
import org.apache.flume.EventDeliveryException;
import org.apache.flume.FlumeException;
import org.apache.flume.api.RpcClient;
import org.apache.flume.api.RpcClientFactory;
import org.apache.flume.event.EventBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GangliaFlumeClient {
    private static final Logger log = LoggerFactory.getLogger(GangliaFlumeClient.class);
    private static final String METRICCONF = "monitor.properties";
    private RpcClient client;
    private String gmetadHostPort;
    private String localHostPort;
    private String groupName;

    public void init() {
        String ip = null;
        String hostname = null;
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
            hostname = InetAddress.getLocalHost().getHostName();
            this.localHostPort = ip + ":" + hostname;
            this.client = getFailOverClient();
        } catch (FlumeException | UnknownHostException fe) {
            log.error(fe.getMessage(), fe);
        }
    }

    public RpcClient getFailOverClient() {
        Properties failOverProps = new Properties();
        Properties metricProps = new Properties();
        try {
            metricProps.load(GangliaFlumeClient.class.getClassLoader().getResourceAsStream(METRICCONF));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        failOverProps.put("client.type", metricProps.getProperty("client.type"));
        failOverProps.put("hosts", metricProps.getProperty("hosts"));
        failOverProps.put("hosts.h1", metricProps.getProperty("hosts.h1"));
        failOverProps.put("hosts.h2", metricProps.getProperty("hosts.h2"));

        this.gmetadHostPort = metricProps.getProperty("hostport");
        this.groupName = metricProps.getProperty("groupname");
        try{
            return RpcClientFactory.getInstance(failOverProps);
        }catch(FlumeException fe){
            log.error("get fail client error!");
        }
        return null;
    }

    public void sendDataToFlume(String data){
        Event event = EventBuilder.withBody(data,Charset.forName("UTF-8"));
        try {
            client.append(event);
        } catch (EventDeliveryException e) {
            log.error("send data to flume error!!!!!!");
            client.close();
            client = null;
            client = getFailOverClient();
        }
    }

    public void sendListToFlume(List<String> list) {
        List<Event> eventList = list.stream().map(data -> EventBuilder.withBody(data, Charset.forName("UTF-8"))).collect(Collectors.toList());
        try {
            client.appendBatch(eventList);
        } catch (EventDeliveryException e) {
            log.error("send data to flume error");
            client.close();
            client = null;
            client = getFailOverClient();
        }
    }

    public void cleanUp() {
        client.close();
    }

    public String getGmetadHostPort() {
        return gmetadHostPort;
    }

    public String getLocalHostPort() {
        return localHostPort;
    }

    public String getGroupName() {
        return groupName;
    }

}
