package com.kingdeehit.bigdata.appmetric.schedule;

import java.util.Map;
import java.util.TimerTask;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.kingdeehit.bigdata.appmetric.flume.GangliaFlumeClient;
import com.kingdeehit.bigdata.appmetric.model.MetricInfo;
import com.kingdeehit.bigdata.appmetric.utils.CachedClient;
import com.kingdeehit.bigdata.appmetric.utils.StringUtil;

public class MetricTimerTask extends TimerTask {

    private static final Logger log = LoggerFactory.getLogger(MetricTimerTask.class);
    private MetricStatistic statstic;
    private GangliaFlumeClient client = new GangliaFlumeClient();

    public MetricTimerTask(MetricStatistic statstic) {
        this.statstic = statstic;
        this.client.init();
    }

    @Override
    public void run() {
        // log.info("start task！！！！");
        long sleepSecond = breakIntervalTime();
        long intervalSecond = sleepSecond + statstic.getIntervalSecond();
        // long intervalSecond = statstic.getIntervalSecond();
        Map<String, String> map = Maps.newHashMap();
        String serviceName = statstic.getServiceName();
        CachedClient cachedClient = statstic.getCachedClient();
        // log.info("metric aggr!!!");
        // 指标聚合发送
        statstic.getItemMap().entrySet().stream().forEach(entry -> {
            // log.info("key:" + entry.getKey() + "|value:" + entry.getValue());
            String metricName = entry.getKey();
            String agrOper = entry.getValue();
            String metricValue = null;
            String redisMetricName = serviceName + "_" + metricName;
                // log.info("redis");
            try {
                metricValue = cachedClient.getCached(redisMetricName);
            } catch (Exception e) {
                log.error("redis getmetric {} error", redisMetricName, e);
            }
                // log.info("agrOper:" + agrOper);

            switch (agrOper) {
            case "SUM":
                if (StringUtils.isNoneBlank(metricValue)) {
                        map.put(serviceName + "." + metricName, metricValue);
                }
                break;
            case "MAX":
            case "AVGSUM":
                if (StringUtils.isNoneBlank(metricValue)) {
                        map.put(serviceName + "." + metricName, metricValue);
                }
                try {
                    cachedClient.setCached(redisMetricName, "0");
                } catch (Exception e) {
                    log.error("redis setmetric {} error", redisMetricName, e);
                }
                break;
            case "AVG":
                if (StringUtils.isNoneBlank(metricValue)) {
                    long metric = Long.parseLong(metricValue);
                        // log.info("avg metricvalue:" + metricValue);
                        // log.info("value:" + metric / intervalSecond);
                        map.put(serviceName + "." + metricName, metric / intervalSecond + "");
                }
                try {
                    cachedClient.setCached(redisMetricName, "0");
                } catch (Exception e) {
                    log.error("redis setmetric {} error", redisMetricName, e);
                }
                break;
            default:
                break;
            }
        });

        // log.info("send data to flume!");
        MetricInfo info = new MetricInfo(serviceName, client.getGmetadHostPort(), client.getLocalHostPort(), client.getGroupName(), map);
        // 以json的形式输入flume
        // log.info(JSON.toJSONString(info, true));
        client.sendDataToFlume(JSON.toJSONString(info));

        // log.info("task over!");
    }

    private long breakIntervalTime() {
        long random = StringUtil.random(1, 10000);
        try {
            Thread.sleep(random);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        }
        return random / 1000;
    }

}
