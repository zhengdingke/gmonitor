package com.kingdeehit.bigdata.appmetric.schedule;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.MoreObjects;
import com.kingdeehit.bigdata.appmetric.utils.StringUtil;

public class MetricSchedule {

    private static final Logger log = LoggerFactory.getLogger(MetricSchedule.class);
    private static final long DEFAULT_DELAY = 60;
    private static final long DEFAULT_INTERVAL = 60;
    private static final long DEFAULT_THREAD = 2;

    private Long delaySecond;
    private Long intervalSecond;
    private MetricStatistic statstic;
    private ScheduledThreadPoolExecutor stpe;

    public MetricSchedule(Integer threadPoolNum, Long delaySecond, Long intervalSecond, MetricStatistic statstic) {
        this.delaySecond = MoreObjects.firstNonNull(delaySecond, DEFAULT_DELAY);
        this.intervalSecond = MoreObjects.firstNonNull(intervalSecond, DEFAULT_INTERVAL);
        this.stpe = new ScheduledThreadPoolExecutor(MoreObjects.firstNonNull(threadPoolNum, DEFAULT_THREAD).intValue());
        this.statstic = statstic;
    }

    public void start() {
        this.statstic.reset();
        this.statstic.setIntervalSecond(intervalSecond);
        stpe.scheduleWithFixedDelay(new MetricTimerTask(statstic), delaySecond, intervalSecond, TimeUnit.SECONDS);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private Long intervalSecond;
        private Integer threadPoolNum;
        private MetricStatistic statstic;
        private Builder() {
        }

        public Builder setThreadPoolNum(Integer threadPoolNum) {
            this.threadPoolNum = threadPoolNum;
            return this;
        }

        public Builder setIntervalSecond(Long intervalSecond) {
            this.intervalSecond = intervalSecond;
            return this;
        }

        public Builder setStatstic(MetricStatistic statstic) {
            this.statstic = statstic;
            return this;
        }

        public MetricSchedule build() {
            long delaySecond = StringUtil.random(50, 100);
            return new MetricSchedule(threadPoolNum, delaySecond, intervalSecond, statstic);
        }

    }
}
