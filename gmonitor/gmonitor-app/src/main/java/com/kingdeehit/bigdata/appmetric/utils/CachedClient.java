package com.kingdeehit.bigdata.appmetric.utils;

import java.util.Map;



public interface CachedClient {
    public void setCached(String key, String value) throws Exception;

    public String getCached(String key) throws Exception;

    public Object getCachedObject(String key) throws Exception;

    public void setCached(String key, Object value) throws Exception;

    public abstract void del(byte[] key) throws Exception;

    public abstract void del(String key) throws Exception;

    public abstract boolean exists(String key) throws Exception;

    public abstract void setCached(String key, Object value, int timeout) throws Exception;

    public abstract void setCached(String key, String value, int timeout) throws Exception;

    public long incr(String key) throws Exception;

    public long incr(String key, int timeout) throws Exception;

    public long incrBy(String key, long incr) throws Exception;

    public long decr(String key) throws Exception;

    public abstract Map<String, String> getHashAll(String key);

    public abstract String getHashCached(String key, String field);

    public abstract void setHash(String key, Map<String, String> map) throws Exception;

    public abstract void setHash(String key, String field, String value) throws Exception;
}
