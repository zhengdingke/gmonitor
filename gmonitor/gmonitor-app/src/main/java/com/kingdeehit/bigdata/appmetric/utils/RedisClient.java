/**
 *
 */
package com.kingdeehit.bigdata.appmetric.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;
import redis.clients.jedis.exceptions.JedisDataException;
import redis.clients.jedis.exceptions.JedisException;

/**
 * redis锟斤拷锟斤拷锟斤拷
 *
 * @author chaoming
 *
 */
public class RedisClient implements CachedClient {

    private static ShardedJedisPool shardedJedisPool;// 锟斤拷片锟斤拷锟接筹拷
	private static Logger logger = Logger.getLogger(RedisClient.class);

	public RedisClient(JedisPoolConfig config, String serverIP, int serverPort, String password) {
		initialShardedPool(config, serverIP, serverPort, password);
	}
	
	private synchronized static void initialShardedPool(JedisPoolConfig config, String serverIP, int serverPort, String password) {
		if (shardedJedisPool == null) {
			JedisShardInfo info = new JedisShardInfo(serverIP, serverPort);
			if (StringUtils.isNotEmpty(password)) {
				info.setPassword(password);
			}			
			List<JedisShardInfo> list = new ArrayList<JedisShardInfo>();
			list.add(info);
			shardedJedisPool = new ShardedJedisPool(config, list);
		}
	}

	@Override
	public void setCached(String key, String value) throws Exception {
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();
			shardedJedis.set(key, value);
		} catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
	}

	@Override
	public void setCached(String key, String value, int timeout) throws Exception {
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();
			shardedJedis.setex(key, timeout, value);			
		} catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
	}

	@Override
	public String getCached(String key) throws Exception {
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();
			String result = shardedJedis.get(key);
			return result;
		} catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
	}

	@Override
	public void del(String key) throws Exception {
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();
			shardedJedis.del(key);
		} catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
	}
	
	@Override
	public void del(byte[] key) throws Exception {
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();			
			shardedJedis.del(key);
		} catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
	}

	@Override
	public boolean exists(String key) throws Exception {
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();
			boolean result = shardedJedis.exists(key);
			return result;
		}  catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
	}
	
	public void sadd(String key, List<Serializable> members) throws Exception {
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();
			for (Serializable member : members) {
				shardedJedis.sadd(key.getBytes(), SerializeUtil.serialize(member));
			}
		} catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
	}

	public List<Object> smembers(String key) throws Exception {
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();
			List<byte[]> list = new ArrayList<byte[]>(shardedJedis.smembers(key.getBytes()));
			List<Object> result = new ArrayList<Object>();
			for(byte[] data : list) {
				result.add(SerializeUtil.unserialize(data));
			}
			return result;
		} catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
	}

	@Override
	public void setCached(String key, Object value) throws Exception {
		if (value == null || key == null) {
			return;
		}
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();
			shardedJedis.set(key.getBytes(), SerializeUtil.serialize(value));
		} catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
	}

	@Override
	public void setCached(String key, Object value, int timeout) throws Exception {
		if (value == null || key == null) {
			return;
		}
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();
			shardedJedis.setex(key.getBytes(), timeout, SerializeUtil.serialize(value));
		} catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
	}

	@Override
	public Object getCachedObject(String key) throws Exception {
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();
			byte[] result = shardedJedis.get(key.getBytes());
			if (result == null) {
				return null;
			}
			Object instance = SerializeUtil.unserialize(result);
			return instance;
		} catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
	}

	@Override
	public long incr(String key) throws Exception {
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();
			boolean isExist = shardedJedis.exists(key);
			if(!isExist){
				shardedJedis.set(key, 0+"");
			}
			long result = shardedJedis.incr(key);			
			return result;
		} catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
	}

	@Override
	public long incr(String key, int timeout) throws Exception {
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();
			boolean isExist = shardedJedis.exists(key);
			if(!isExist){
				shardedJedis.setex(key, timeout, 0+"");
			}
			long result = shardedJedis.incr(key);
			return result;
		} catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
	}

	@Override
	public long decr(String key) throws Exception {
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();
			boolean isExist = shardedJedis.exists(key);
			long result = 0;
			if(isExist){
				result = shardedJedis.decr(key);
			}		
			return result;
		} catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
	}
	
	        /**
     * 锟斤拷锟絡edisException
     * 
     * @param jedisException
     * @return
     */
	protected boolean handleJedisException(JedisException jedisException) {
	    if (jedisException instanceof JedisConnectionException) {
	        logger.error("Redis connection lost.", jedisException);
	    } else if (jedisException instanceof JedisDataException) {
	        if ((jedisException.getMessage() != null) && (jedisException.getMessage().indexOf("READONLY") != -1)) {
	            logger.error("Redis connection are read-only slave.", jedisException);
	        } else {
	            // dataException, isBroken=false
	            return false;
	        }
	    } else {
	        logger.error("Jedis exception happen.", jedisException);
	    }
	    return true;
	}
	
	/**
	 * 
	 * @param shardedJedis
	 * @param conectionBroken
	 */
	protected void closeResourse(ShardedJedis shardedJedis, boolean conectionBroken) {
		try {
	        if (conectionBroken) {
	        	shardedJedisPool.returnBrokenResource(shardedJedis);
	        } else {
	        	shardedJedisPool.returnResource(shardedJedis);
	        }
	    } catch (Exception e) {
	        logger.error("return back jedis failed, will fore close the jedis.", e);
	    }
	}
	
	@Override
	public void setHash(String key, String field, String value) throws Exception {
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();
			shardedJedis.hset(key, field, value);			
		} catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
	}
	
	@Override
	public void setHash(String key, Map<String, String> map) throws Exception {
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();
			shardedJedis.hmset(key, map);
		} catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
	}
	
	
	@Override
	public String getHashCached(String key, String field) {
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();
			String result = shardedJedis.hget(key, field);
			return result;
		} catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
	}
	
	@Override
	public Map<String, String> getHashAll(String key) {
		ShardedJedis shardedJedis = null;
		boolean broken = false;
		try {
			shardedJedis = shardedJedisPool.getResource();
			Map<String, String> result = shardedJedis.hgetAll(key);
			return result;
		} catch (JedisException e) {
			broken = handleJedisException(e);
			throw e;
		} finally{
			closeResourse(shardedJedis, broken);
		}
    }

    @Override
    public long incrBy(String key, long incr) throws Exception {
        ShardedJedis shardedJedis = null;
        boolean broken = false;
        try {
            shardedJedis = shardedJedisPool.getResource();
            boolean isExist = shardedJedis.exists(key);
            if (!isExist) {
                shardedJedis.set(key, 0 + "");
            }
            long result = shardedJedis.incrBy(key, incr);
            return result;
        } catch (JedisException e) {
            broken = handleJedisException(e);
            throw e;
        } finally {
            closeResourse(shardedJedis, broken);
        }
    }
	
}
