package com.kingdeehit.bigdata.appmetric.model;

import java.util.Map;

import com.google.common.collect.Maps;

public class MetricInfo {

    private String serviceName;
    private String gmetadInfo;
    private String ipHost;
    private String groupName;

    private Map<String, String> map = Maps.newHashMap();

    public MetricInfo() {
    }

    public MetricInfo(String serviceName, String gmetadInfo, String ipHost, String groupName, Map<String, String> map) {
        this.serviceName = serviceName;
        this.gmetadInfo = gmetadInfo;
        this.ipHost = ipHost;
        this.groupName = groupName;
        this.map = map;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getIpHost() {
        return ipHost;
    }

    public void setIpHost(String ipHost) {
        this.ipHost = ipHost;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Map<String, String> getMap() {
        return map;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }

    public String getGmetadInfo() {
        return gmetadInfo;
    }

    public void setGmetadInfo(String gmetadInfo) {
        this.gmetadInfo = gmetadInfo;
    }

}
