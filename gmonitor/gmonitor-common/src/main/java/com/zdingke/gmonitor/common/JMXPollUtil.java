package com.zdingke.gmonitor.common;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanServerConnection;
import javax.management.ObjectInstance;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class JMXPollUtil {
    private static final Log LOG = LogFactory.getLog(JMXPollUtil.class);

    private static Set<ObjectInstance> getAllBeans(String host, String port) throws IOException {
        MBeanServerConnection mbeanServer = createMBeanServerConn(host, port);
        Set<ObjectInstance> queryMBeans = null;
        try {
            queryMBeans = mbeanServer.queryMBeans(null, null);
        } catch (Exception ex) {
            LOG.error("Could not get Mbeans for monitoring", ex);
        }
        return queryMBeans;
    }

    public static Map<String, Map<String, String>> getAllMBeansByType(String host, String port, String objType) throws IOException {
        MBeanServerConnection mbeanServer = createMBeanServerConn(host, port);
        Map<String, Map<String, String>> mbeanMap = Maps.newHashMap();
        for (ObjectInstance obj : getAllBeans(host, port)) {
            try {
                if (!obj.getObjectName().toString().startsWith(objType)) {
                    continue;
                }
                MBeanAttributeInfo[] attrs = mbeanServer.getMBeanInfo(obj.getObjectName()).getAttributes();
                String strAtts[] = new String[attrs.length];
                for (int i = 0; i < strAtts.length; i++) {
                    strAtts[i] = attrs[i].getName();
                }
                AttributeList attrList = mbeanServer.getAttributes(obj.getObjectName(), strAtts);
                String component = obj.getObjectName().toString().substring(obj.getObjectName().toString().indexOf('=') + 1);
                Map<String, String> attrMap = Maps.newHashMap();

                for (Object attr : attrList) {
                    Attribute localAttr = (Attribute) attr;
                    if (localAttr.getName().equalsIgnoreCase("type")) {
                        component = localAttr.getValue() + "." + component;
                    }
                    attrMap.put(localAttr.getName(), localAttr.getValue().toString());
                }
                mbeanMap.put(component, attrMap);
            } catch (Exception e) {
                LOG.error("Unable to poll JMX for metrics.", e);
            }
        }

        return mbeanMap;
    }

    public static List<String> getAllObjByType(String host, String port, String objType) throws IOException {
        List<String> objList = Lists.newArrayList();
        for (ObjectInstance obj : getAllBeans(host, port)) {
            try {
                if (!obj.getObjectName().toString().startsWith(objType)) {
                    continue;
                }
                objList.add(obj.getObjectName().toString());
            } catch (Exception e) {
                LOG.error("Unable to poll JMX for metrics.", e);
            }
        }
        return objList;
    }

    private static MBeanServerConnection createMBeanServerConn(String host, String port) throws IOException {
        String url = "service:jmx:rmi:///jndi/rmi://" + host + ":" + port + "/jmxrmi";
        JMXServiceURL serviceUrl = new JMXServiceURL(url);
        JMXConnector jmxConnector = JMXConnectorFactory.connect(serviceUrl, null);
        return  jmxConnector.getMBeanServerConnection();
    }

    public static void main(String[] args) throws IOException {
        if (args.length != 3) {
            LOG.info("param <host,jmxport,jmxtype>");
        } else {
            String host = args[0];
            String jmxport = args[1];
            String jmxtype = args[2];
            Map<String, Map<String, String>> map = JMXPollUtil.getAllMBeansByType(host, jmxport, jmxtype);
            map.entrySet().stream().filter(e -> e.getKey().contains("jap")).forEach(m -> {
                System.out.println("-----obj:" + m.getKey());
                m.getValue().entrySet().forEach(v -> {
                    System.out.println("-------------" + v.getKey());
                    System.out.println("-------------" + v.getValue());
                });
            });
        }
    }
}
