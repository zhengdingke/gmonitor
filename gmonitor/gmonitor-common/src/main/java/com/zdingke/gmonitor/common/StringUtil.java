package com.zdingke.gmonitor.common;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.common.collect.Maps;

public class StringUtil {

    public static List<String> zkObjMatch(List<String> objList, String matchStr) {
        return objList.stream().filter(obj -> obj.equals(matchStr)).collect(Collectors.toList());
    }

    public static List<String> match(List<String> objList, String matchStr) {
        return objList.stream().filter(obj -> obj.startsWith(matchStr)).collect(Collectors.toList());
    }

    public static Map<String, String> objMatch(List<String> objList, String matchStr) {
        Map<String,String> result = Maps.newHashMap();
        // objList.stream().forEach(o -> System.out.println("obj:" + o));
        // System.out.println("match:" + matchStr);
        objList.stream().filter(obj -> obj.startsWith(matchStr)).forEach(o -> {
            // System.out.println("key:" + o.replace(matchStr, ""));
            // System.out.println("value:" + o);
            result.put(o.replace(matchStr, ""), o);
        });
        return result;
    }

    public static String tranFirstCharUpper(String fildeName) {
        byte[] items = fildeName.getBytes();
        items[0] = (byte) ((char) items[0] - 'a' + 'A');
        return new String(items);
    }

    public static List<File> getJsonFiles(String dirpath) {
        File[] files = null;
        File dirFile = new File(dirpath);
        if ((dirFile != null) && (dirFile.isFile())) {
            files = new File[1];
            files[0] = dirFile;
        } else {
            files = dirFile.listFiles();
        }
        List<File> result = new ArrayList();
        for (File file : files) {
            if (isJsonFile(file)) {
                result.add(file);
            }
        }
        return result;
    }

    private static boolean isJsonFile(File file) {
        return (file.isFile()) && (file.getName().endsWith(".json"));
    }
}
