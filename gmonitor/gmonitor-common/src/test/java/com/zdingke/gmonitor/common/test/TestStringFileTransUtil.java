package com.zdingke.gmonitor.common.test;


import java.io.File;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.zdingke.gmonitor.common.StringFileTransUtil;

public class TestStringFileTransUtil {

    @Test
    public void file2String() {
        String x = StringFileTransUtil.file2String(new File(TestStringFileTransUtil.class.getResource("/").getFile().toString() + "nimbus.json"), "GBK");
        System.out.println(JSON.toJSON(x));
    }

    @Test
    public void string2File() {
        String x = StringFileTransUtil.file2String(new File(TestStringFileTransUtil.class.getResource("/").getFile().toString() + "nimbus.json"), "GBK");
        StringFileTransUtil.string2File(x, new File(TestStringFileTransUtil.class.getResource("/").getFile().toString() + "nimbus1.json"));
    }

    @Test
    public void testGetStringFromMetric() {
        String comstr = StringFileTransUtil.getComponentInfo("tomcat", "E:/eclipse/eclipse_pro/kingee_pro/bigdata/ganglia-metric/src/test/resources/test");
        System.out.println("comstr:" + comstr);
    }

}
