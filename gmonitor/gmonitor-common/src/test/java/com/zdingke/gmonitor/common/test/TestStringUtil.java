package com.zdingke.gmonitor.common.test;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import com.zdingke.gmonitor.common.JMXPollUtil;
import com.zdingke.gmonitor.common.StringUtil;

public class TestStringUtil {

    @Test
    public void testKafkaMatchObj() throws IOException {
        List<String> objList = JMXPollUtil.getAllObjByType("119.29.121.79", "9999", "kafka.server");
        String str = "kafka.server:type=BrokerTopicMetrics,name=TotalFetchRequestsPerSec,topic=*";
        StringUtil.match(objList, str.substring(str.indexOf("=") + 1).replace("*", ""))
                .stream().forEach(s -> System.out.println(s));
    }

    @Test
    public void testFlumeMatchObj() throws IOException {
        List<String> objList = JMXPollUtil.getAllObjByType("119.29.121.79", "5445", "org.apache.flume");
        String str = "org.apache.flume.channel:type=*";
        StringUtil.match(objList, str.substring(str.indexOf("=") + 1).replace("*", "")).stream().forEach(s -> System.out.println(s));
    }

    @Test
    public void testFirstTrans() {
        System.out.println(StringUtil.tranFirstCharUpper("dingke"));
    }
}
