package com.zdingke.gmonitor.common.test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.zdingke.gmonitor.common.JMXPollUtil;

public class TestJMXPollUtil {

    @Test
    public void testKafkaJmx() throws IOException {
        Map<String, Map<String, String>> map = JMXPollUtil.getAllMBeansByType("119.29.121.79", "9999", "kafka.server");
        map.entrySet().forEach(m -> {
            System.out.println(m.getKey());
            m.getValue().entrySet().forEach(v -> {
                System.out.println(v.getKey());
                System.out.println(v.getValue());
            });
        });
        System.out.println(map.size());
    }
    
    @Test
    public void testGetAllObj() throws IOException {
        List<String> objList = JMXPollUtil.getAllObjByType("119.29.28.72", "9999", "kafka.server");
        objList.stream().forEach(o -> {
            System.out.println(o);
        });
    }

    @Test
    public void testFlumeJmx() throws IOException {
        Map<String, Map<String, String>> map = JMXPollUtil.getAllMBeansByType("119.29.121.79", "5445", "org.apache.flume");
        map.entrySet().forEach(m -> {
            System.out.println(m.getKey());
            m.getValue().entrySet().forEach(v -> {
                System.out.println(v.getKey());
                System.out.println(v.getValue());
            });
        });
        System.out.println(map.size());
    }

    @Test
    public void testTomcatJmx() throws Exception {
        Map<String, Map<String, String>> map = JMXPollUtil.getAllMBeansByType("203.195.240.163", "7080", "Catalina");
        map.entrySet().forEach(m -> {
            System.out.println(m.getKey());
            m.getValue().entrySet().forEach(v -> {
                System.out.println(v.getKey());
                System.out.println(v.getValue());
            });
        });
    }
}
