<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
	<span><br />
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		<span>说明：</span>
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		基于jmxtrans，主要针对ganglia与hadoop组件的指标监控，json配置支持动态更新，支持指标匹配规则自定义
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		<br />
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		使用方式（前提搭建好ganglia和要监控的组件）：
	</div>
	<ul style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		<li>
			lib中的jar包配置进classpath
		</li>
		<li>
			修改conf目录下的配置文件
		</li>
		<li>
			创建${service}-jvmmetric.json，${service}-metric.json，${service}-info.json三种配置文件，具体配置方式可以查看项目json下的例子
		</li>
		<li>
			创建${service}MetricsJson类，继承MetricsJson，重写objMatch，getServiceType，getJmxType
		</li>
	</ul>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		<br />
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		<span>实例解析（kafka监控）：</span>
	</div>
	<ul style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		<li>
			组件信息
		</li>
	</ul>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>kafka-info.json：</span>
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		{
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; "componentname": "kafka",
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; "gmondConn": "1.1.1.1:8649",
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; "module": [
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; &nbsp; &nbsp; {
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; "host": "1.1.1.1",
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; "jmxport": "9999",
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; "jvmInclude": true,
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; "name": "m1.kafka",
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; "type": "kafka"
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; &nbsp; &nbsp; }
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; ]
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		}
	</div>
	<ul style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		<li>
			组件指标
		</li>
	</ul>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp;&nbsp;&nbsp;kafka-metric.json{<br />
&nbsp; &nbsp; "metrics": [<br />
&nbsp; &nbsp; &nbsp; &nbsp; {<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; "obj": "kafka.server:type=BrokerTopicMetrics,name=TotalFetchRequestsPerSec,topic=*",<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; "resultAlias": "${name}.broker.*.TotalFetchRequestsPerSec",<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; "attr": [<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; "MeanRate"<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ]<br />
&nbsp; &nbsp; &nbsp; &nbsp; }<br />
&nbsp; &nbsp; ]<br />
}<br />
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		<br />
	</div>
	<ul style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		<li>
			组件jvm指标
		</li>
	</ul>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp;&nbsp;&nbsp;&nbsp;<span>kafka-jvmmetric.json：</span>
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp;&nbsp;&nbsp;&nbsp;{
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; "metrics": [
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; &nbsp; &nbsp; {
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; "obj": "java.lang:type=Memory",
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; "resultAlias": "${name}.memory",
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; "attr": [
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; "HeapMemoryUsage",
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; "NonHeapMemoryUsage"
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ]
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; &nbsp; &nbsp; }
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		&nbsp; &nbsp; ]
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		}
	</div>
	<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
		<br />
	</div>
</span>
</div>
<div style="color:#5C5C5C;font-family:&quot;font-size:15px;background-color:#FFFFFF;">
</div>

KafkaMetricsJson.class:

    @Override
    public List<MetricInfo> objMatch(List<String> objList, MetricInfo m, String host) {
        List<MetricInfo> afterMatchList = Lists.newArrayList();
        String matchObj = m.getObj();
        if (matchObj.endsWith("*")) {
            Map<String, String> map = StringUtil.objMatch(objList, matchObj.replace("*", ""));
            map.entrySet().stream().forEach(e -> {
                afterMatchList.add(new MetricInfo(e.getValue(), m.getAttr(), m.getResultAlias().replace("*", e.getKey())));
            });
        } else {
            afterMatchList.add(new MetricInfo(matchObj, m.getAttr(), m.getResultAlias()));
        }
        return afterMatchList;
    }

    @Override
    public String getServiceType() {
        return "kafka";
    }

    @Override
    public String getJmxType() {
        return "kafka.server";
    }

