package com.zdingke.gmonitor.utils.test;

import java.io.IOException;

import org.junit.Test;

import com.zdingke.gmonitor.utils.ZKUtil;

public class TestZKUtil {

    @Test
    public void testGetZKMode() throws NumberFormatException, IOException, InterruptedException {
        ZKUtil zk = new ZKUtil("119.29.28.72:2181");
        System.out.println(zk.getZKMode());
    }

    @Test
    public void testGetServerId() throws InterruptedException, IOException {
        ZKUtil zk = new ZKUtil("119.29.28.72:2181");
        System.out.println(zk.getServerId());
    }
}
