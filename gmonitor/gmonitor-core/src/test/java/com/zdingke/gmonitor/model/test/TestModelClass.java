package com.zdingke.gmonitor.model.test;

import java.io.File;
import java.util.Arrays;
import java.util.Date;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.zdingke.gmonitor.common.StringFileTransUtil;
import com.zdingke.gmonitor.model.ComponentInfo;
import com.zdingke.gmonitor.model.JVMInfo;
import com.zdingke.gmonitor.model.MetricInfo;
import com.zdingke.gmonitor.model.ModuleInfo;
import com.zdingke.gmonitor.model.RequestContent;

public class TestModelClass {

    @Test
    public void generateComponentJsonFile() {
        MetricInfo minfo = new MetricInfo("memory", Arrays.asList(new String[]{"memory1","memory2"}),"memory");
        ModuleInfo moinfo = new ModuleInfo("nimbus", "nb", "1.1.1.1", "111", Arrays.asList(minfo));
        ComponentInfo cinfo = new ComponentInfo("storm", Arrays.asList(moinfo), "1.1.1.1:1111");
        StringFileTransUtil.string2File(JSON.toJSONString(cinfo, true), new File(TestModelClass.class.getResource("/").getFile().toString() + "storm.json"));
    }
    
    @Test
    public void generateJvmJosnFile(){
        MetricInfo minfo = new MetricInfo("memory", Arrays.asList(new String[]{"memory1","memory2"}),"memory");
        JVMInfo jinfo = new JVMInfo();

        jinfo.setMetrics(Arrays.asList(minfo));
        StringFileTransUtil.string2File(JSON.toJSONString(jinfo, true), new File(TestModelClass.class.getResource("/").getFile().toString() + "jvm.json"));
    }
    
    @Test
    public void combineJvm() {
        String jvm = StringFileTransUtil.file2String(new File(TestModelClass.class.getResource("/").getFile().toString() + "jvm.json"), "GBK");
        JVMInfo jinfo = JSON.parseObject(jvm, JVMInfo.class);
        jinfo.getMetrics().stream().forEach(m -> System.out.println(m.toString()));
    }

    @Test
    public void replaceName() {
        String jvm = StringFileTransUtil.file2String(new File(TestModelClass.class.getResource("/").getFile().toString() + "jvm.json"), "GBK");
        System.out.println(jvm);
        System.out.println(jvm.replace("${name}", "nimbus"));
    }

    @Test
    public void testRequestContentToString() {
        RequestContent rc = new RequestContent("problem", "127.0.0.1", "CHECKGANGLIA CRITICAL: load_one is 3.56");
        System.out.println(rc.toString());
    }

    @Test
    public void testRequestContentDataTransform() {
        Date date1 = new Date("Mon Nov 28 18:01:52 CST 2016");
        System.out.println(date1.toLocaleString());
    }
}
