package com.zdingke.gmonitor.utils.test;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.zdingke.gmonitor.MetricsJson;
import com.zdingke.gmonitor.utils.MetricJsonCreateUtil;

public class TestMetricJsonCreateUtil {

    @Test
    public void testGetMetricsJsonByServiceType() {
        MetricsJson metric = MetricJsonCreateUtil.getMetricsJsonByServiceType("kafka",
                "E:/eclipse/eclipse_pro/kingee_pro/bigdata/ganglia-metric/target/test-classes/unittest");
        System.out.println(metric.getServiceType());
    }

    @Test
    public void testGetModuleJsonObjByServiceType() {
        MetricJsonCreateUtil.getModuleJsonObjByServiceType("kafka", "E:/eclipse/eclipse_pro/kingee_pro/bigdata/ganglia-metric/target/test-classes/unittest")
                .stream().forEach(j -> System.out.println("result:" + JSON.toJSONString(j, true)));
    }
}
