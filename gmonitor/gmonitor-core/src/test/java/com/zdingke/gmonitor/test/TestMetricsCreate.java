package com.zdingke.gmonitor.test;

import java.io.IOException;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zdingke.gmonitor.FlumeMetricsJson;
import com.zdingke.gmonitor.KafkaMetricsJson;
import com.zdingke.gmonitor.MetricsJson;
import com.zdingke.gmonitor.StormMetricsJson;
import com.zdingke.gmonitor.TomcatMetricsJson;
import com.zdingke.gmonitor.ZookeeperMetricsJson;

public class TestMetricsCreate {

    @Test
    public void testCreateJSONObj(){
        JSONObject settings = new JSONObject();
        settings.put("groupName", "kafka");
        settings.put("host", "10.104.151.80");
        settings.put("port", "8649");
       
        JSONObject write = new JSONObject();
        write.put("@class", "com.googlecode.jmxtrans.model.output.GangliaWriter");
        write.put("settions", settings);
        
        JSONArray outputWriters = new JSONArray();
        outputWriters.add(write);
        
        JSONArray attr = new JSONArray();
        attr.add("HeapMemoryUsage");
        JSONObject querie = new JSONObject();
        
        querie.put("outputWriters", outputWriters);
        querie.put("obj", "java.lang:type=Memory");
        querie.put("resultAlias", "m2.kafka.memory");
        querie.put("attr", attr);
        
        JSONArray queries = new JSONArray();
        queries.add(querie);
        
        JSONObject server = new JSONObject();
        server.put("port", "9999");
        server.put("host", "10.104.141.206");
        server.put("numQueryThreads", "1");
        server.put("queries", queries);

        JSONArray servers = new JSONArray();
        servers.add(server);

        JSONObject obj = new JSONObject();
        obj.put("servers", servers);
        System.out.println(JSON.toJSONString(obj, true));
    }

    @Test
    public void testStormJson() throws IOException {
        MetricsJson create = new StormMetricsJson("");
        create.createJsonFile();
    }

    @Test
    public void testKafkaJson() throws IOException {
        MetricsJson create = new KafkaMetricsJson("E:/eclipse/eclipse_pro/kingee_pro/bigdata/ganglia-metric/target/test-classes/unittest");
        create.createJsonFile();
    }

    @Test
    public void testFlumeJson() throws IOException {
        MetricsJson create = new FlumeMetricsJson("E:/eclipse/eclipse_pro/kingee_pro/bigdata/ganglia-metric/target/test-classes/unittest");
        create.createJsonFile();
    }

    @Test
    public void testZKJson() throws IOException {
        MetricsJson create = new ZookeeperMetricsJson("");
        create.createJsonFile();
    }

    @Test
    public void testTomcatJson() throws IOException {
        MetricsJson create = new TomcatMetricsJson("E:/eclipse/eclipse_pro/kingee_pro/bigdata/ganglia-metric/src/test/resources/test/");
        create.createJsonFile();
    }
}
