package com.googlecode.jmxtrans.util;

import java.io.File;
import java.io.IOException;
import java.util.List;

import name.pachler.nio.file.ClosedWatchServiceException;
import name.pachler.nio.file.FileSystems;
import name.pachler.nio.file.Path;
import name.pachler.nio.file.Paths;
import name.pachler.nio.file.StandardWatchEventKind;
import name.pachler.nio.file.WatchEvent;
import name.pachler.nio.file.WatchKey;
import name.pachler.nio.file.WatchService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WatchDir extends Thread {
    private static final Logger log = LoggerFactory.getLogger(WatchDir.class);
    private WatchService watchService = null;
    private WatchedCallback watched = null;

    public WatchDir(File dir, WatchedCallback watched) throws IOException {
        this.watched = watched;
        this.watchService = FileSystems.getDefault().newWatchService();
        Path watchedPath = Paths.get(dir.getAbsolutePath());
        watchedPath.register(this.watchService, new WatchEvent.Kind[] { StandardWatchEventKind.ENTRY_CREATE, StandardWatchEventKind.ENTRY_DELETE,
                StandardWatchEventKind.ENTRY_MODIFY });
    }

    @Override
    public void run() {
        for (;;) {
            WatchKey signalledKey;
            try {
                signalledKey = this.watchService.take();
            } catch (InterruptedException ix) {
                continue;
            } catch (ClosedWatchServiceException cwse) {
                log.debug("Watch service closed, terminating.");
                break;
            }
            List<WatchEvent<?>> list = signalledKey.pollEvents();

            signalledKey.reset();
            try {
                for (WatchEvent<?> e : list) {
                    if (e.kind() == StandardWatchEventKind.ENTRY_CREATE) {
                        Path context = (Path) e.context();
                        this.watched.fileAdded(new File(context.toString()));
                    } else if (e.kind() == StandardWatchEventKind.ENTRY_DELETE) {
                        Path context = (Path) e.context();
                        this.watched.fileDeleted(new File(context.toString()));
                    } else if (e.kind() == StandardWatchEventKind.ENTRY_MODIFY) {
                        Path context = (Path) e.context();
                        this.watched.fileModified(new File(context.toString()));
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void stopService() throws Exception {
        if (this.watchService != null) {
            this.watchService.close();
        }
    }
}
