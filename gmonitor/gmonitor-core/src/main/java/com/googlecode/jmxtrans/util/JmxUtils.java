package com.googlecode.jmxtrans.util;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.reflect.Array;
import java.rmi.UnmarshalException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularDataSupport;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.pool.KeyedObjectPool;
import org.apache.commons.pool.KeyedPoolableObjectFactory;
import org.apache.commons.pool.impl.GenericKeyedObjectPool;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.SerializationConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.googlecode.jmxtrans.OutputWriter;
import com.googlecode.jmxtrans.jmx.ManagedObject;
import com.googlecode.jmxtrans.model.JmxProcess;
import com.googlecode.jmxtrans.model.Query;
import com.googlecode.jmxtrans.model.Result;
import com.googlecode.jmxtrans.model.Server;

public class JmxUtils {
    private static final Logger log = LoggerFactory.getLogger(JmxUtils.class);

    public static void mergeServerLists(List<Server> existing, List<Server> adding) {
        for (Server server : adding) {
            Server found;
            if (existing.contains(server)) {
                found = existing.get(existing.indexOf(server));

                List<Query> queries = server.getQueries();
                for (Query q : queries) {
                    try {
                        found.addQuery(q);
                    } catch (ValidationException ex) {
                        log.error("Error adding query: " + q + " to server" + server, ex);
                    }
                }
            } else {
                existing.add(server);
            }
        }
    }

    public static void processQueriesForServer(MBeanServerConnection mbeanServer, Server server) throws Exception {
        if (server.isQueriesMultiThreaded()) {
            ExecutorService service = null;
            try {
                service = Executors.newFixedThreadPool(server.getNumQueryThreads().intValue());
                if (log.isDebugEnabled()) {
                    log.debug("----- Creating " + server.getQueries().size() + " query threads");
                }
                List<Callable<Object>> threads = new ArrayList(server.getQueries().size());
                for (Query query : server.getQueries()) {
                    query.setServer(server);
                    ProcessQueryThread pqt = new ProcessQueryThread(mbeanServer, query);
                    threads.add(Executors.callable(pqt));
                }
                service.invokeAll(threads);
            } finally {
                shutdownAndAwaitTermination(service);
            }
        } else {
            for (Query query : server.getQueries()) {
                query.setServer(server);
                processQuery(mbeanServer, query);
            }
        }
    }

    private static void shutdownAndAwaitTermination(ExecutorService service) {
        service.shutdown();
        try {
            if (!service.awaitTermination(60L, TimeUnit.SECONDS)) {
                service.shutdownNow();
                if (!service.awaitTermination(60L, TimeUnit.SECONDS)) {
                    log.error("Pool did not terminate");
                }
            }
        } catch (InterruptedException ie) {
            service.shutdownNow();

            Thread.currentThread().interrupt();
        }
    }

    public static class ProcessQueryThread implements Runnable {
        private MBeanServerConnection mbeanServer;
        private Query query;

        public ProcessQueryThread(MBeanServerConnection mbeanServer, Query query) {
            this.mbeanServer = mbeanServer;
            this.query = query;
        }

        @Override
        public void run() {
            try {
                JmxUtils.processQuery(this.mbeanServer, this.query);
            } catch (Exception e) {
                JmxUtils.log.error("Error executing query", e);
                throw new RuntimeException(e);
            }
        }
    }

    public static void processQuery(MBeanServerConnection mbeanServer, Query query) throws Exception {
        ObjectName oName = new ObjectName(query.getObj());
        Set<ObjectName> queryNames = mbeanServer.queryNames(oName, null);
        for (ObjectName queryName : queryNames) {
            List<Result> resList = new ArrayList();

            MBeanInfo info = mbeanServer.getMBeanInfo(queryName);
            ObjectInstance oi = mbeanServer.getObjectInstance(queryName);

            List<String> queryAttributes = query.getAttr();
            if ((queryAttributes == null) || (queryAttributes.size() == 0)) {
                MBeanAttributeInfo[] attrs = info.getAttributes();
                for (MBeanAttributeInfo attrInfo : attrs) {
                    query.addAttr(attrInfo.getName());
                }
            }
            try {
                if ((query.getAttr() != null) && (query.getAttr().size() > 0)) {
                    if (log.isDebugEnabled()) {
                        log.debug("Executing queryName: " + queryName.getCanonicalName() + " from query: " + query);
                    }
                    AttributeList al = mbeanServer.getAttributes(queryName, query.getAttr().toArray(new String[query.getAttr().size()]));
                    for (Attribute attribute : al.asList()) {
                        getResult(resList, info, oi, attribute, query);
                    }
                    query.setResults(resList);

                    runOutputWritersForQuery(query);
                    if (log.isDebugEnabled()) {
                        log.debug("Finished running outputWriters for query: " + query);
                    }
                }
            } catch (UnmarshalException ue) {
                if ((ue.getCause() != null) && ((ue.getCause() instanceof ClassNotFoundException))) {
                    log.debug(
                            "Bad unmarshall, continuing. This is probably ok and due to something like this: http://ehcache.org/xref/net/sf/ehcache/distribution/RMICacheManagerPeerListener.html#52",
                            ue.getMessage());
                }
            }
        }
    }

    private static void getResult(List<Result> resList, MBeanInfo info, ObjectInstance oi, String attributeName, CompositeData cds, Query query) {
        CompositeType t = cds.getCompositeType();

        Result r = getNewResultObject(info, oi, attributeName, query);

        Set<String> keys = t.keySet();
        for (String key : keys) {
            Object value = cds.get(key);
            if ((value instanceof TabularDataSupport)) {
                TabularDataSupport tds = (TabularDataSupport) value;
                processTabularDataSupport(resList, info, oi, r, attributeName + "." + key, tds, query);
                r.addValue(key, value);
            } else {
                if ((value instanceof CompositeDataSupport)) {
                    CompositeDataSupport cds2 = (CompositeDataSupport) value;
                    getResult(resList, info, oi, attributeName, cds2, query);
                    return;
                }
                r.addValue(key, value);
            }
        }
        resList.add(r);
    }

    private static void processTabularDataSupport(List<Result> resList, MBeanInfo info, ObjectInstance oi, Result r, String attributeName,
            TabularDataSupport tds, Query query) {
        Set<Map.Entry<Object, Object>> entries = tds.entrySet();
        for (Map.Entry<Object, Object> entry : entries) {
            Object entryKeys = entry.getKey();
            if ((entryKeys instanceof List)) {
                StringBuilder sb = new StringBuilder();
                for (Object entryKey : (List) entryKeys) {
                    sb.append(".");
                    sb.append(entryKey);
                }
                String attributeName2 = sb.toString();
                Object entryValue = entry.getValue();
                if ((entryValue instanceof CompositeDataSupport)) {
                    getResult(resList, info, oi, attributeName + attributeName2, (CompositeDataSupport) entryValue, query);
                } else {
                    throw new RuntimeException("!!!!!!!!!! Please file a bug: https://github.com/lookfirst/jmxtrans/issues entryValue is: "
                            + entryValue.getClass().getCanonicalName());
                }
            } else {
                throw new RuntimeException("!!!!!!!!!! Please file a bug: https://github.com/lookfirst/jmxtrans/issues entryKeys is: "
                        + entryKeys.getClass().getCanonicalName());
            }
        }
    }

    private static Result getNewResultObject(MBeanInfo info, ObjectInstance oi, String attributeName, Query query) {
        Result r = new Result(attributeName);
        r.setQuery(query);
        r.setClassName(info.getClassName());
        r.setTypeName(oi.getObjectName().getCanonicalKeyPropertyListString());
        return r;
    }

    private static void getResult(List<Result> resList, MBeanInfo info, ObjectInstance oi, Attribute attribute, Query query) {
        Object value = attribute.getValue();
        if (value != null) {
            if ((value instanceof CompositeDataSupport)) {
                getResult(resList, info, oi, attribute.getName(), (CompositeData) value, query);
            } else if ((value instanceof CompositeData[])) {
                for (CompositeData cd : (CompositeData[]) value) {
                    getResult(resList, info, oi, attribute.getName(), cd, query);
                }
            } else if ((value instanceof ObjectName[])) {
                Result r = getNewResultObject(info, oi, attribute.getName(), query);
                for (ObjectName obj : (ObjectName[]) value) {
                    r.addValue(obj.getCanonicalName(), obj.getKeyPropertyListString());
                }
                resList.add(r);
            } else if (value.getClass().isArray()) {
                Result r = getNewResultObject(info, oi, attribute.getName(), query);
                for (int i = 0; i < Array.getLength(value); i++) {
                    Object val = Array.get(value, i);
                    r.addValue(attribute.getName() + "." + i, val);
                }
                resList.add(r);
            } else if ((value instanceof TabularDataSupport)) {
                TabularDataSupport tds = (TabularDataSupport) value;
                Result r = getNewResultObject(info, oi, attribute.getName(), query);
                processTabularDataSupport(resList, info, oi, r, attribute.getName(), tds, query);
                resList.add(r);
            } else {
                Result r = getNewResultObject(info, oi, attribute.getName(), query);
                r.addValue(attribute.getName(), value);
                resList.add(r);
            }
        }
    }

    private static void runOutputWritersForQuery(Query query) throws Exception {
        List<OutputWriter> writers = query.getOutputWriters();
        if (writers != null) {
            for (OutputWriter writer : writers) {
                writer.doWrite(query);
            }
        }
    }

    public static JMXConnector getServerConnection(Server server) throws Exception {
        JMXServiceURL url = new JMXServiceURL(server.getUrl());
        if ((server.getProtocolProviderPackages() != null) && (server.getProtocolProviderPackages().contains("weblogic"))) {
            return JMXConnectorFactory.connect(url, getWebLogicEnvironment(server));
        }
        return JMXConnectorFactory.connect(url, getEnvironment(server));
    }

    public static Map<String, String> getWebLogicEnvironment(Server server) {
        Map<String, String> environment = new HashMap();
        String username = server.getUsername();
        String password = server.getPassword();
        if ((username != null) && (password != null))
        {
            environment.put("jmx.remote.protocol.provider.pkgs", server.getProtocolProviderPackages());
            environment.put("java.naming.security.principal", username);
            environment.put("java.naming.security.credentials", password);
        }
        return environment;
    }

    public static Map<String, String[]> getEnvironment(Server server) {
        Map<String, String[]> environment = new HashMap();
        String username = server.getUsername();
        String password = server.getPassword();
        if ((username != null) && (password != null))
        {
            String[] credentials = new String[2];
            credentials[0] = username;
            credentials[1] = password;

            environment.put("jmx.remote.credentials", credentials);
        }
        return environment;
    }

    public static void execute(JmxProcess process) throws Exception {
        List<JMXConnector> conns = new ArrayList();
        if (process.isServersMultiThreaded()) {
            ExecutorService service = null;
            try {
                service = Executors.newFixedThreadPool(process.getNumMultiThreadedServers().intValue());
                for (Server server : process.getServers()) {
                    JMXConnector conn = getServerConnection(server);
                    conns.add(conn);
                    service.execute(new ProcessServerThread(server, conn));
                }
                service.shutdown();
            } finally {
                try {
                    service.awaitTermination(60000L, TimeUnit.SECONDS);
                } catch (InterruptedException ex) {
                    log.error("Error shutting down execution.", ex);
                }
            }
        } else {
            for (Server server : process.getServers()) {
                JMXConnector conn = getServerConnection(server);
                conns.add(conn);
                processServer(server, conn);
            }
        }
        for (JMXConnector conn : conns) {
            try {
                conn.close();
            } catch (Exception ex) {
                log.error("Error closing connection.", ex);
            }
        }
    }

    public static class ProcessServerThread implements Runnable {
        private Server server;
        private JMXConnector conn;

        public ProcessServerThread(Server server, JMXConnector conn)
        {
            this.server = server;
            this.conn = conn;
        }

        @Override
        public void run()
        {
            try {
                JmxUtils.processServer(this.server, this.conn);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void processServer(Server server, JMXConnector conn) throws Exception {
        MBeanServerConnection mbeanServer = conn.getMBeanServerConnection();
        processQueriesForServer(mbeanServer, server);
    }

    public static void printJson(JmxProcess process) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.getSerializationConfig().set(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
        System.out.println(mapper.writeValueAsString(process));
    }

    public static void prettyPrintJson(JmxProcess process) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.getSerializationConfig().set(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
        ObjectWriter writer = mapper.defaultPrettyPrintingWriter();
        System.out.println(writer.writeValueAsString(process));
    }

    public static JmxProcess getJmxProcess(File file) throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        JmxProcess jmx = mapper.readValue(file, JmxProcess.class);
        jmx.setName(file.getName());
        return jmx;
    }

    public static boolean isNumeric(Object value) {
        return ((value instanceof Number)) || (((value instanceof String)) && (isNumeric((String) value)));
    }

    public static boolean isNumeric(String str) {
        if (StringUtils.isEmpty(str)) {
            return str != null;
        }
        int decimals = 0;
        int sz = str.length();
        for (int i = 0; i < sz; i++) {
            char cat = str.charAt(i);
            if (cat == '.') {
                decimals++;
                if (decimals == 1) {
                }
            } else if (!Character.isDigit(cat)) {
                return false;
            }
        }
        return decimals < str.length();
    }

    public static <T extends KeyedPoolableObjectFactory> GenericKeyedObjectPool getObjectPool(T factory) {
        GenericKeyedObjectPool pool = new GenericKeyedObjectPool(factory);
        pool.setTestOnBorrow(true);
        pool.setMaxActive(-1);
        pool.setMaxIdle(-1);
        pool.setTimeBetweenEvictionRunsMillis(300000L);
        pool.setMinEvictableIdleTimeMillis(300000L);

        return pool;
    }

    public static Map<String, KeyedObjectPool> getDefaultPoolMap() {
        Map<String, KeyedObjectPool> poolMap = new HashMap();

        GenericKeyedObjectPool pool = getObjectPool(new SocketFactory());
        poolMap.put(Server.SOCKET_FACTORY_POOL, pool);

        GenericKeyedObjectPool jmxPool = getObjectPool(new JmxConnectionFactory());
        poolMap.put(Server.JMX_CONNECTION_FACTORY_POOL, jmxPool);

        GenericKeyedObjectPool dsPool = getObjectPool(new DatagramSocketFactory());
        poolMap.put(Server.DATAGRAM_SOCKET_FACTORY_POOL, dsPool);

        return poolMap;
    }

    public static void registerJMX(ManagedObject mbean) throws Exception {
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        mbs.registerMBean(mbean, mbean.getObjectName());
    }

    public static void unregisterJMX(ManagedObject mbean) throws Exception {
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        mbs.unregisterMBean(mbean.getObjectName());
    }

    public static String getKeyString(Query query, Result result, Map.Entry<String, Object> values, List<String> typeNames, String rootPrefix) {
        String keyStr = null;
        if (values.getKey().startsWith(result.getAttributeName())) {
            keyStr = values.getKey();
        } else {
            keyStr = result.getAttributeName() + "." + values.getKey();
        }
        String alias = null;
        if (query.getServer().getAlias() != null) {
            alias = query.getServer().getAlias();
        } else {
            alias = query.getServer().getHost() + "_" + query.getServer().getPort();
            alias = cleanupStr(alias);
        }
        StringBuilder sb = new StringBuilder();
        if (rootPrefix != null) {
            sb.append(rootPrefix);
            sb.append(".");
        }
        sb.append(alias);
        sb.append(".");
        if (result.getClassNameAlias() != null) {
            sb.append(result.getClassNameAlias());
        } else {
            sb.append(cleanupStr(result.getClassName()));
        }
        sb.append(".");

        String typeName = cleanupStr(getConcatedTypeNameValues(query, typeNames, result.getTypeName()));
        if ((typeName != null) && (typeName.length() > 0)) {
            sb.append(typeName);
            sb.append(".");
        }
        sb.append(cleanupStr(keyStr));

        return sb.toString();
    }

    public static String getKeyString2(Query query, Result result, Map.Entry<String, Object> values, List<String> typeNames, String rootPrefix) {
        String keyStr = null;
        if (values.getKey().startsWith(result.getAttributeName())) {
            keyStr = values.getKey();
        } else {
            keyStr = result.getAttributeName() + "." + values.getKey();
        }
        StringBuilder sb = new StringBuilder();
        if (result.getClassNameAlias() != null) {
            sb.append(result.getClassNameAlias());
        } else {
            sb.append(cleanupStr(result.getClassName()));
        }
        sb.append(".");

        String typeName = cleanupStr(getConcatedTypeNameValues(query, typeNames, result.getTypeName()));
        if ((typeName != null) && (typeName.length() > 0)) {
            sb.append(typeName);
            sb.append(".");
        }
        sb.append(cleanupStr(keyStr));

        return sb.toString();
    }

    public static String cleanupStr(String name) {
        if (name == null) {
            return null;
        }
        String clean = name.replace(".", "_");
        clean = clean.replace(" ", "");
        clean = clean.replace("\"", "");
        clean = clean.replace("'", "");
        return clean;
    }

    public static String getConcatedTypeNameValues(List<String> typeNames, String typeNameStr) {
        if ((typeNames == null) || (typeNames.size() == 0)) {
            return null;
        }
        String[] tokens = typeNameStr.split(",");
        StringBuilder sb = new StringBuilder();
        for (String key : typeNames) {
            String result = getTypeNameValue(key, tokens);
            if (result != null) {
                sb.append(result);
                sb.append("_");
            }
        }
        return StringUtils.chomp(sb.toString(), "_");
    }

    public static String getConcatedTypeNameValues(Query query, List<String> typeNames, String typeName) {
        Set<String> queryTypeNames = query.getTypeNames();
        if ((queryTypeNames != null) && (queryTypeNames.size() > 0)) {
            List<String> allNames = new ArrayList(queryTypeNames);
            for (String name : typeNames) {
                if (!allNames.contains(name)) {
                    allNames.add(name);
                }
            }
            return getConcatedTypeNameValues(allNames, typeName);
        }
        return getConcatedTypeNameValues(typeNames, typeName);
    }

    private static String getTypeNameValue(String typeName, String[] tokens) {
        boolean foundIt = false;
        for (String token : tokens) {
            String[] keys = token.split("=");
            for (String key : keys) {
                if (foundIt) {
                    return key;
                }
                if (typeName.equals(key)) {
                    foundIt = true;
                }
            }
        }
        return null;
    }
}
