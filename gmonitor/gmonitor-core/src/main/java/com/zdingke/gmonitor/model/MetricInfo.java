package com.zdingke.gmonitor.model;

import java.util.List;

import com.alibaba.fastjson.JSON;

public class MetricInfo {

    private String obj;
    private List<String> attr;
    private String resultAlias;

    public MetricInfo() {

    }

    public MetricInfo(String obj, List<String> attr, String resultAlias) {
        this.obj = obj;
        this.attr = attr;
        this.resultAlias = resultAlias;
    }

    public String getObj() {
        return obj;
    }

    public void setObj(String obj) {
        this.obj = obj;
    }

    public List<String> getAttr() {
        return attr;
    }

    public void setAttr(List<String> attr) {
        this.attr = attr;
    }

    public String getResultAlias() {
        return resultAlias;
    }

    public void setResultAlias(String resultAlias) {
        this.resultAlias = resultAlias;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }

}
