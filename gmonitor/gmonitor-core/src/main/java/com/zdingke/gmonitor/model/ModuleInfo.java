package com.zdingke.gmonitor.model;

import java.util.List;

public class ModuleInfo {

    private String type;
    private String name;
    private String host;
    private String jmxport;
    private List<MetricInfo> metrics;
    private boolean isJvmInclude = true;

    public ModuleInfo() {
    }

    public ModuleInfo(String type, String name, String host, String jmxport, List<MetricInfo> metrics) {
        this.type = type;
        this.name = name;
        this.host = host;
        this.jmxport = jmxport;
        this.metrics = metrics;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getJmxport() {
        return jmxport;
    }

    public void setJmxport(String jmxport) {
        this.jmxport = jmxport;
    }

    public List<MetricInfo> getMetrics() {
        return metrics;
    }

    public void setMetrics(List<MetricInfo> metrics) {
        this.metrics = metrics;
    }

    public boolean isJvmInclude() {
        return isJvmInclude;
    }

    public void setJvmInclude(boolean isJvmInclude) {
        this.isJvmInclude = isJvmInclude;
    }

}
