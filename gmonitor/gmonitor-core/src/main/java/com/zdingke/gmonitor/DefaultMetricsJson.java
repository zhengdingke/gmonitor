package com.zdingke.gmonitor;

import java.util.List;

import com.google.common.collect.Lists;
import com.zdingke.gmonitor.model.MetricInfo;

public class DefaultMetricsJson extends MetricsJson {

    public DefaultMetricsJson(String dir) {
        super(dir);
    }

    @Override
    public List<MetricInfo> objMatch(List<String> objList, MetricInfo m, String host) {
        return Lists.newArrayList();
    }

    @Override
    public String getServiceType() {
        return "default";
    }

    @Override
    public String getJmxType() {
        return "default";
    }

}
