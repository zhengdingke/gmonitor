package com.zdingke.gmonitor;


import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.zdingke.gmonitor.common.JMXPollUtil;
import com.zdingke.gmonitor.common.StringFileTransUtil;
import com.zdingke.gmonitor.model.ComponentInfo;
import com.zdingke.gmonitor.model.ComponentMetricInfo;
import com.zdingke.gmonitor.model.JVMInfo;
import com.zdingke.gmonitor.model.MetricInfo;
import com.zdingke.gmonitor.model.ModuleInfo;

public abstract class MetricsJson {

    private static final Log LOG = LogFactory.getLog(MetricsJson.class);
    private String dir;

    public MetricsJson(String dir) {
        this.dir = dir;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public ComponentInfo includeJVMMetric(ComponentInfo ci) {
        ci.getModule().stream().filter(f -> f.isJvmInclude()).forEach(module -> {
            JVMInfo jvmInfo = JSON.parseObject(StringFileTransUtil.getJvmMetricInfo(getServiceType(), dir), JVMInfo.class);
            jvmInfo.getMetrics().stream().forEach(j -> {
                j.setResultAlias(j.getResultAlias().replace("${name}", module.getName()));
            });
            if (module.getMetrics() == null) {
                module.setMetrics(jvmInfo.getMetrics());
            } else {
                module.getMetrics().addAll(jvmInfo.getMetrics());
            }
        });
        return ci;
    }

    public ComponentInfo includeComponentMetric(ComponentInfo ci, List<MetricInfo> mList) throws IOException {
        ci.getModule().stream().forEach(module -> {
            List<MetricInfo> metricList = generateAllMetrics(mList, module.getHost(), module.getJmxport());
            metricList.stream().forEach(m -> {
                m.setResultAlias(m.getResultAlias().replace("${name}", module.getName()));
            });
            module.setMetrics(metricList);
        });
        return ci;
    }

    private List<MetricInfo> generateAllMetrics(List<MetricInfo> mList, String host, String port) {
        List<MetricInfo> ml = Lists.newArrayList();

        mList.stream().forEach(m -> {
            List<String> objList = Lists.newArrayList();
            try {
                objList = JMXPollUtil.getAllObjByType(host, port, getJmxType());
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            }
            if (!objList.isEmpty()) {
                ml.addAll(objMatch(objList, m, host));
            }
        });
        return ml;
    }

    public JSONObject createJsonObject(String gmondConn, String serviceType, ModuleInfo minfo) {
        String[] gmonds = gmondConn.split(":");
        String gmondhost = gmonds[0];
        String gmondport = gmonds[1];

        JSONArray queries = new JSONArray();
        minfo.getMetrics().stream().forEach(m -> {
            JSONObject settings = new JSONObject();
            settings.put("groupName", serviceType);
            settings.put("host", gmondhost);
            settings.put("port", gmondport);

            JSONObject write = new JSONObject();
            write.put("@class", "com.googlecode.jmxtrans.model.output.GangliaWriter");
            write.put("settings", settings);

            JSONArray outputWriters = new JSONArray();
            outputWriters.add(write);

            JSONObject querie = new JSONObject();
            querie.put("outputWriters", outputWriters);
            querie.put("obj", m.getObj());
            querie.put("resultAlias", m.getResultAlias());
            JSONArray attr = new JSONArray();
            attr.addAll(m.getAttr());
            querie.put("attr", attr);
            queries.add(querie);
        });

        JSONObject server = new JSONObject();
        server.put("port", minfo.getJmxport());
        server.put("host", minfo.getHost());
        server.put("numQueryThreads", "1");
        server.put("queries", queries);

        JSONArray servers = new JSONArray();
        servers.add(server);

        JSONObject obj = new JSONObject();
        obj.put("servers", servers);

        return obj;
    }


    public void createJsonFile() throws IOException {
        String dirpath = dir.endsWith("/") ? dir.substring(0, dir.length() - 1) : dir;
        String serviceType = getServiceType();
        ComponentInfo cinfo = combineInfoAndMetric(dirpath, serviceType);

        cinfo.getModule().stream().forEach(m -> {
            JSONObject moduleObj = createJsonObject(cinfo.getGmondConn(), serviceType, m);
            String jsonFileName = m.getName() + ".json";
            boolean over = StringFileTransUtil.string2File(JSON.toJSONString(moduleObj, true), new File(dir + "/test/" + jsonFileName));
            LOG.info("createFile:" + dir + ":" + jsonFileName + ":" + (over ? "success" : "false"));
        });
    };

    public ComponentInfo combineInfoAndMetric(String dir, String serviceType) throws IOException {
        String comstr = StringFileTransUtil.getComponentInfo(serviceType, dir);
        String mestr = StringFileTransUtil.getMetricInfo(serviceType, dir);
        ComponentInfo cinfo = JSON.parseObject(comstr, ComponentInfo.class);
        if (StringUtils.isNotBlank(mestr)) {
            List<MetricInfo> mList = JSON.parseObject(mestr, ComponentMetricInfo.class).getMetrics();
            includeComponentMetric(cinfo, mList);
        }
        return includeJVMMetric(cinfo);
    }

    /*
     * objList:表示jmx过滤掉不符合前缀（比如org.apache.zookeepr）之后的obj集合
     * matchObj:json中需要进行匹配的obj
     */
    public abstract List<MetricInfo> objMatch(List<String> objList, MetricInfo m, String host);

    public abstract String getServiceType();

    public abstract String getJmxType();
}
