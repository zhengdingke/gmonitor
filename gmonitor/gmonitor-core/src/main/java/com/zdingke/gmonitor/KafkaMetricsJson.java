package com.zdingke.gmonitor;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.zdingke.gmonitor.common.StringUtil;
import com.zdingke.gmonitor.model.MetricInfo;

public class KafkaMetricsJson extends MetricsJson {

    public KafkaMetricsJson(String dir) {
        super(dir);
    }

    private String REPLACE_STR = "*";

    /*
     * "obj": "kafka.server:type=BrokerTopicMetrics,name=BytesOutPerSec,topic=*"
     */
    @Override
    public List<MetricInfo> objMatch(List<String> objList, MetricInfo m, String host) {
        List<MetricInfo> afterMatchList = Lists.newArrayList();
        String matchObj = m.getObj();
        if (matchObj.endsWith(REPLACE_STR)) {
            Map<String, String> map = StringUtil.objMatch(objList, matchObj.replace(REPLACE_STR, ""));
            map.entrySet().stream().forEach(e -> {
                afterMatchList.add(new MetricInfo(e.getValue(), m.getAttr(), m.getResultAlias().replace(REPLACE_STR, e.getKey())));
            });
        } else {
            afterMatchList.add(new MetricInfo(matchObj, m.getAttr(), m.getResultAlias()));
        }
        return afterMatchList;
    }

    @Override
    public String getServiceType() {
        return "kafka";
    }

    @Override
    public String getJmxType() {
        return "kafka.server";
    }

}
