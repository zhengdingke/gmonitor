package com.zdingke.gmonitor.utils;

import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.client.FourLetterWordMain;

import com.zdingke.gmonitor.common.StringUtil;

public class ZKUtil {

    private static final Log LOG = LogFactory.getLog(ZKUtil.class);
    private ZooKeeper zk;
    private String hostAndIp;
    private final String KAFAK_PATITIONS_NUM_PATH = "/brokers/topics/${topic}/partitions";

    public ZKUtil(String hostAndIp) throws InterruptedException, IOException {
        // connectToZK();
        this.hostAndIp = hostAndIp;
    }

    public void connectToZK() throws InterruptedException, IOException {
        if (zk != null && zk.getState().isAlive()) {
            zk.close();
        }
        zk = new ZooKeeper(hostAndIp, 60 * 1000, new MyWatcher());
    }

    private class MyWatcher implements Watcher {
        @Override
        public void process(WatchedEvent event) {
            LOG.info(event.toString());
        }
    }

    public String execFourLetter(String letter) throws NumberFormatException, IOException {
        String[] hi = hostAndIp.split(":");
        return FourLetterWordMain.send4LetterWord(hi[0], Integer.parseInt(hi[1]), letter);
    }

    public String getZKMode() throws NumberFormatException, IOException {
        String srvr = execFourLetter("srvr");
        String modestr = Stream.of(srvr.split("\n")).filter(f -> f.contains("Mode")).collect(Collectors.toList()).get(0);
        return StringUtil.tranFirstCharUpper(modestr.substring(modestr.indexOf(":") + 1).trim());
    }

    public String getServerId() throws NumberFormatException, IOException {
        String conf = execFourLetter("conf");
        String modestr = Stream.of(conf.split("\n")).filter(f -> f.contains("serverId")).collect(Collectors.toList()).get(0);
        return modestr.substring(modestr.indexOf("=") + 1).trim();
    }

    public String getKafkaPartitionNum(String topic) throws KeeperException, InterruptedException {
        return new String(zk.getData(KAFAK_PATITIONS_NUM_PATH.replace("${topic}", topic), null, null));
    }
}
