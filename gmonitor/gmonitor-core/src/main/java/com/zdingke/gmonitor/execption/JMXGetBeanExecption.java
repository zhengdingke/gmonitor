package com.zdingke.gmonitor.execption;

public class JMXGetBeanExecption extends Exception {
    private static final long serialVersionUID = 1L;

    public JMXGetBeanExecption(String message) {
        super(message);
    }

    public JMXGetBeanExecption(String message, Exception e) {
        super(message, e);
    }
}
