package com.zdingke.gmonitor.notification;

import java.io.IOException;
import java.util.Properties;
import java.util.stream.Stream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.zdingke.gmonitor.common.HttpRequest;
import com.zdingke.gmonitor.model.CloudHomeRequestInfo;
import com.zdingke.gmonitor.model.RequestContent;
import com.zdingke.gmonitor.model.RequestMessageData;
import com.zdingke.gmonitor.utils.XStreamUtil;

public class CloudHomeInfoSend {

    private static final Log log = LogFactory.getLog(CloudHomeInfoSend.class);
    private static final String NOTIFYCONF = "nagios_notify.properties";
    public static void main(String[] args) {
        String openId = args[0];// split by ,

        Properties notifyProps = new Properties();
        try {
            notifyProps.load(CloudHomeInfoSend.class.getClassLoader().getResourceAsStream(NOTIFYCONF));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        RequestContent content = new RequestContent(args[1], args[2], args[3]);

        RequestMessageData msgData = new RequestMessageData("��Ϣ����", content.toString());
        CloudHomeRequestInfo info = new CloudHomeRequestInfo(null, null, notifyProps.getProperty("gangliaurl"), notifyProps.getProperty("appname"), "1",
                msgData);

        HttpRequest request = new HttpRequest();
        Stream.of(openId.split(",")).forEach(p -> {
            try {
                info.setWorkOpenId(p);
                request.sendPostRequest(notifyProps.getProperty("apiurl"), XStreamUtil.toXML(info));
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        });
    }
}
