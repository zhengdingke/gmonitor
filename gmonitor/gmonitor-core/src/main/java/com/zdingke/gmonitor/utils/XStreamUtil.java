package com.zdingke.gmonitor.utils;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.zdingke.gmonitor.model.CloudHomeRequestInfo;


public class XStreamUtil {

    public static String toXML(CloudHomeRequestInfo obj) {
        XStream xStream = new XStream(new DomDriver("utf-8"));
        return xStream.toXML(obj);
    }
}
