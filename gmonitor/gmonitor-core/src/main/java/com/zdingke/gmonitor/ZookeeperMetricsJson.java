package com.zdingke.gmonitor;

import java.io.IOException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.collect.Lists;
import com.zdingke.gmonitor.common.StringUtil;
import com.zdingke.gmonitor.model.MetricInfo;
import com.zdingke.gmonitor.utils.ZKUtil;

public class ZookeeperMetricsJson extends MetricsJson {

    private static final Log LOG = LogFactory.getLog(ZookeeperMetricsJson.class);
    private final String PORT = "2181";

    public ZookeeperMetricsJson(String dir) {
        super(dir);
    }
    /*
     * "obj":
     * "org.apache.ZooKeeperService:name0=ReplicatedServer_id${serverid},name1=replica.${serverid},name2=${mode}"
     */
    @Override
    public List<MetricInfo> objMatch(List<String> objList, MetricInfo m, String host) {
        List<MetricInfo> afterMatchList = Lists.newArrayList();
        String matchObj = m.getObj();
        try {
            ZKUtil zk = new ZKUtil(host + ":" + PORT);
            String serverid = zk.getServerId();
            String mode = zk.getZKMode();
            if (matchObj.contains("${serverid}")) {
                List<String> list = StringUtil.zkObjMatch(objList, matchObj.replace("${serverid}", serverid).replace("${mode}", mode));
                list.stream().forEach(obj -> {
                    afterMatchList.add(new MetricInfo(obj, m.getAttr(), m.getResultAlias()));
                });
            } else {
                afterMatchList.add(new MetricInfo(matchObj, m.getAttr(), m.getResultAlias()));
            }
        } catch (InterruptedException | IOException e) {
            LOG.error(e.getMessage(), e);
        }
        return afterMatchList;
    }

    @Override
    public String getServiceType() {
        return "zookeeper";
    }

    @Override
    public String getJmxType() {
        return "org.apache.ZooKeeperService";
    }

}
