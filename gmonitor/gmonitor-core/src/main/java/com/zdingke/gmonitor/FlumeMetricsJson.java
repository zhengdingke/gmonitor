package com.zdingke.gmonitor;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.zdingke.gmonitor.common.StringUtil;
import com.zdingke.gmonitor.model.MetricInfo;

public class FlumeMetricsJson extends MetricsJson {

    private String REPLACE_STR = "*";

    public FlumeMetricsJson(String dir) {
        super(dir);
    }
    /*
     * "obj": "org.apache.flume.channel:type=*", "obj":
     * "org.apache.flume.source:type=*", "obj": "org.apache.flume.sink:type=*"
     */
    @Override
    public List<MetricInfo> objMatch(List<String> objList, MetricInfo m, String host) {
        List<MetricInfo> afterMatchList = Lists.newArrayList();
        String matchObj = m.getObj();
        if (matchObj.endsWith("*")) {
            Map<String, String> map = StringUtil.objMatch(objList, matchObj.replace(REPLACE_STR, ""));
            map.entrySet().stream().forEach(e -> {
                afterMatchList.add(new MetricInfo(e.getValue(), m.getAttr(), m.getResultAlias().replace(REPLACE_STR, e.getKey())));
            });
        } else {
            afterMatchList.add(new MetricInfo(matchObj, m.getAttr(), m.getResultAlias()));
        }
        return afterMatchList;
    }

    @Override
    public String getServiceType() {
        return "flume";
    }

    @Override
    public String getJmxType() {
        return "org.apache.flume";
    }
}
