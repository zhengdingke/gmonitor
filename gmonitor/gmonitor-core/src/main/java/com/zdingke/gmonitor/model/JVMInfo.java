package com.zdingke.gmonitor.model;

import java.util.List;

public class JVMInfo {
    List<MetricInfo> metrics;

    public JVMInfo() {
        super();
    }

    public List<MetricInfo> getMetrics() {
        return metrics;
    }

    public void setMetrics(List<MetricInfo> metrics) {
        this.metrics = metrics;
    }
}
