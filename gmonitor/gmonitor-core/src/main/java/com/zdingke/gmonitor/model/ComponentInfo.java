package com.zdingke.gmonitor.model;

import java.util.List;

public class ComponentInfo {

    private String componentname;
    private List<ModuleInfo> module;
    private String gmondConn;

    public ComponentInfo() {
    }

    public ComponentInfo(String componentname, List<ModuleInfo> module, String gmondConn) {
        this.componentname = componentname;
        this.module = module;
        this.gmondConn = gmondConn;
    }

    public String getComponentname() {
        return componentname;
    }

    public void setComponentname(String componentname) {
        this.componentname = componentname;
    }

    public List<ModuleInfo> getModule() {
        return module;
    }

    public void setModule(List<ModuleInfo> module) {
        this.module = module;
    }

    public String getGmondConn() {
        return gmondConn;
    }

    public void setGmondConn(String gmondConn) {
        this.gmondConn = gmondConn;
    }

}
