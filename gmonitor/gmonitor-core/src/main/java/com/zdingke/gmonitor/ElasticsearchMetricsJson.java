package com.zdingke.gmonitor;

import java.util.List;

import com.google.common.collect.Lists;
import com.zdingke.gmonitor.model.MetricInfo;

public class ElasticsearchMetricsJson extends MetricsJson {


    public ElasticsearchMetricsJson(String dir) {
        super(dir);
    }

    @Override
    public List<MetricInfo> objMatch(List<String> objList, MetricInfo m, String host) {
        return Lists.newArrayList();
    }

    @Override
    public String getServiceType() {
        return "elasticsearch";
    }

    @Override
    public String getJmxType() {
        return null;
    }
}
