package com.zdingke.gmonitor.model;

public class CloudHomeRequestInfo {
    private String empId;
    private String workOpenId;
    private String url;
    private String lightApp;
    private String messageType;
    private RequestMessageData messageData;

    public CloudHomeRequestInfo() {
    }

    public CloudHomeRequestInfo(String empId, String workOpenId, String url, String lightApp, String messageType, RequestMessageData messageData) {
        this.empId = empId;
        this.workOpenId = workOpenId;
        this.url = url;
        this.lightApp = lightApp;
        this.messageType = messageType;
        this.messageData = messageData;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getWorkOpenId() {
        return workOpenId;
    }

    public void setWorkOpenId(String workOpenId) {
        this.workOpenId = workOpenId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLightApp() {
        return lightApp;
    }

    public void setLightApp(String lightApp) {
        this.lightApp = lightApp;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public RequestMessageData getMessageData() {
        return messageData;
    }

    public void setMessageData(RequestMessageData messageData) {
        this.messageData = messageData;
    }
}
