package com.zdingke.gmonitor;


import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;
import com.zdingke.gmonitor.common.StringUtil;
import com.zdingke.gmonitor.model.MetricInfo;

public class TomcatMetricsJson extends MetricsJson {

    private String REPLACE_STR = "*";
    private String FILTER_AJP = "ajp";
    private static List<String> filterApps = Lists.newArrayList();
    
    static {
        filterApps.add("Catalina:type=Manager,context=/,host=localhost");
        filterApps.add("Catalina:type=Manager,context=/docs,host=localhost");
        filterApps.add("Catalina:type=Manager,context=/examples,host=localhost");
        filterApps.add("Catalina:type=Manager,context=/host-manager,host=localhost");
        filterApps.add("Catalina:type=Manager,context=/manager,host=localhost");
    }

    public TomcatMetricsJson(String dir) {
        super(dir);
    }

    @Override
    public List<MetricInfo> objMatch(List<String> objList, MetricInfo m, String host) {

        List<MetricInfo> afterMatchList = Lists.newArrayList();
        List<String> afterFilterList = objList.stream().filter(o -> !filterApps.contains(o)).filter(o -> !o.contains(FILTER_AJP)).collect(Collectors.toList());
        String matchObj = m.getObj();
        if (matchObj.endsWith("*")) {
            Map<String, String> map = StringUtil.objMatch(afterFilterList, matchObj.replace(REPLACE_STR, ""));
            map.entrySet().stream().forEach(e -> {
                String key = e.getKey();
                afterMatchList.add(new MetricInfo(e.getValue(), m.getAttr(), m.getResultAlias().replace(REPLACE_STR, key.substring(1, key.length() - 1))));
            });
        } else {
            afterMatchList.add(new MetricInfo(matchObj, m.getAttr(), m.getResultAlias()));
        }
        return afterMatchList;
    }

    @Override
    public String getServiceType() {
        return "tomcat";
    }

    @Override
    public String getJmxType() {
        return "Catalina";
    }
}
