package com.zdingke.gmonitor.utils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.zdingke.gmonitor.FlumeMetricsJson;
import com.zdingke.gmonitor.KafkaMetricsJson;
import com.zdingke.gmonitor.MetricsJson;
import com.zdingke.gmonitor.StormMetricsJson;
import com.zdingke.gmonitor.ZookeeperMetricsJson;
import com.zdingke.gmonitor.common.StringUtil;
import com.zdingke.gmonitor.model.ComponentInfo;

public class MetricJsonCreateUtil {

    private static final Log LOG = LogFactory.getLog(MetricJsonCreateUtil.class);
    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            LOG.info("parm include <serviceType,jsonDir>");
        } else {
            String serviceType = args[0];
            String dir = args[1];
            if (serviceType.equals("all")) {
                getAllModuleJsonObj(dir).stream().forEach(m -> {
                    System.out.println(JSON.toJSONString(m, true));
                });
            } else {
                getModuleJsonObjByServiceType(serviceType, dir).stream().forEach(m -> {
                    System.out.println(JSON.toJSONString(m, true));
                });
                ;
            }
        }
    }

    public static void storm(String dir) throws IOException {
        MetricsJson create = new StormMetricsJson(dir);
        create.createJsonFile();
    }

    public static void kafka(String dir) throws IOException {
        MetricsJson create = new KafkaMetricsJson(dir);
        create.createJsonFile();
    }

    public static void flume(String dir) throws IOException {
        MetricsJson create = new FlumeMetricsJson(dir);
        create.createJsonFile();
    }

    public static void zookeeper(String dir) throws IOException {
        MetricsJson create = new ZookeeperMetricsJson(dir);
        create.createJsonFile();
    }

    public static List<JSONObject> getAllModuleJsonObj(String dir) {
        Map<Boolean,List<File>> map = StringUtil.getJsonFiles(dir).stream().collect(Collectors.partitioningBy(j -> j.getName().contains("info")));
        String dirpath = dir.endsWith("/") ? dir.substring(dir.length() - 1) : dir;
        List<JSONObject> jsonObj = Lists.newArrayList();
        map.get(true).stream().forEach(info->{
            String serviceType = info.getName().replace("-info.json", "");
            MetricsJson metricJson = getMetricsJsonByServiceType(serviceType, dirpath);
            ComponentInfo cinfo = null;
            try {
                cinfo = metricJson.combineInfoAndMetric(dirpath, serviceType);
                String gmondConn = cinfo.getGmondConn();
                cinfo.getModule().stream().forEach(m -> {
                    jsonObj.add(metricJson.createJsonObject(gmondConn, serviceType, m));
                });
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            }
        });
        return jsonObj;
    }

    public static List<JSONObject> getModuleJsonObjByServiceType(String serviceType, String dir) {
        String dirpath = dir.endsWith("/") ? dir.substring(dir.length() - 1) : dir;
        List<JSONObject> jsonObj = Lists.newArrayList();
        MetricsJson metricJson = getMetricsJsonByServiceType(serviceType, dirpath);
        ComponentInfo cinfo = null;
        try {
            cinfo = metricJson.combineInfoAndMetric(dirpath, serviceType);
            String gmondConn = cinfo.getGmondConn();
            cinfo.getModule().stream().forEach(m -> {
                jsonObj.add(metricJson.createJsonObject(gmondConn, serviceType, m));
            });
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return jsonObj;
    }

    public static MetricsJson getMetricsJsonByServiceType(String serviceType, String dirpath) {
        Class claz = null;
        try {
            claz = Class.forName("com.kingdeehit.bigdata.metric." + StringUtil.tranFirstCharUpper(serviceType) + "MetricsJson");
            Constructor<?> cons[] = claz.getConstructors();
            return (MetricsJson) cons[0].newInstance(dirpath);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            LOG.error(serviceType + "MetricsJson class is not exist!!!!", e);
        }
        return null;
    }
}
