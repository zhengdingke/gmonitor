package com.zdingke.gmonitor.model;

import java.util.List;

public class ComponentMetricInfo {
    List<MetricInfo> metrics;

    public ComponentMetricInfo() {
        super();
    }

    public List<MetricInfo> getMetrics() {
        return metrics;
    }

    public void setMetrics(List<MetricInfo> metrics) {
        this.metrics = metrics;
    }
}
