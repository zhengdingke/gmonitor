package com.zdingke.gmonitor.model;


public class RequestContent {
    private String service;
    private String host;
    private String detail;

    public RequestContent() {
    }

    public RequestContent(String service, String host, String detail) {
        this.service = service;
        this.host = host;
        this.detail = detail;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("service:").append(service).append("\n");
        sb.append("host:").append(host).append("\n");
        sb.append("detail:").append(detail.split(":")[1]);
        return sb.toString();
    }

}
